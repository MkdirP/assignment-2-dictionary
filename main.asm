section .text

%include 'colon.inc'
section .data
%include'words.inc'

error:
    db 'Error!',10, 0
nosuch:
    db 'No such words in the dic', 10, 0
found:
    db 'Words: ', 0

global _start

extern exit
extern string_length
extern print_string
extern read_string
extern find_word

_start:
    mov rdi, rsp
    mov rsi, 256
    call read_string
    test rax, rax
    jz .err
    mov rsi, first
    mov rdi, rsp
    call find_word
    add rsp, 256
    test rax, rax
    jz .nosuch
    push rax
    mov rdi, 1
    mov rsi, found
    call print_string
    pop rax
    add rax, 8
    mov rdi, rax
    push rax
    call string_length
    pop rsi
    add rsi, rax
    inc rsi
    mov rdi, 1
    call print_string
    call exit
.err:
    add rsp, 256
    mov rdi, 2
    mov rsi, error
    call print_string
    call exit
.nosuch
    mov rdi, 2
    mov rsi, nosuch
    call print_string
    call exit

    


