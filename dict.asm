section .text

global find_word

extern string_length
extern string_equals

find_word:
.loop: 
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi
    test rax, rax
    jnz .found
    mov rsi, [rsi]
    test rsi, rsi
    jnz .loop
    xor rax, rax
    ret
.found:
    mov rax, rsi
    ret
