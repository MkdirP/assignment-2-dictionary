%define first 0

%macro colon 2
    %%next: dq first
    %define first %%next
    db %1, 0                ;%1:key
    %2:                     ;%2:name of the label
%endmacro
